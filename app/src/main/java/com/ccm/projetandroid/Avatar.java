package com.ccm.projetandroid;

import java.util.LinkedList;
import java.util.List;

public class Avatar {
    private String name;
    private String ownerHardwareId;
    private Integer limitTime;
    private Integer limitDistance;
    private String currentHardwareId;
    private Integer refreshLocationFrequency;
    private double latitude;
    private double longitude;
    private String isActive;
    private List<Trip> trips;


    private Avatar() {}


    public Avatar(String name, String androidId, Integer limitTime, Integer limitDistance, Integer refreshLocationFrequency, double latitude, double longitude, String isActive) {
        this.name = name;
        this.ownerHardwareId = androidId;
        this.limitTime = limitTime;
        this.limitDistance = limitDistance;
        this.currentHardwareId = androidId;
        this.refreshLocationFrequency = refreshLocationFrequency;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isActive = isActive;
        this.trips = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerHardwareId() {
        return ownerHardwareId;
    }

    public void setOwnerHardwareId(String ownerHardwareId) {
        this.ownerHardwareId = ownerHardwareId;
    }

    public String getCurrentHardwareId() {
        return currentHardwareId;
    }

    public void setCurrentHardwareId(String currentHardwareId) {
        this.currentHardwareId = currentHardwareId;
    }

    public Integer getLimitTime() {
        return limitTime;
    }

    public void setLimitTime(Integer limitTime) {
        this.limitTime = limitTime;
    }

    public Integer getLimitDistance() {
        return limitDistance;
    }

    public void setLimitDistance(Integer limitDistance) {
        this.limitDistance = limitDistance;
    }


    public Integer getRefreshLocationFrequency() {
        return refreshLocationFrequency;
    }

    public void setRefreshLocationFrequency(Integer refreshLocationFrequency) {
        this.refreshLocationFrequency = refreshLocationFrequency;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
