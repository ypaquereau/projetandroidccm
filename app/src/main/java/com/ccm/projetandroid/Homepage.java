package com.ccm.projetandroid;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.io.IOException;

public class Homepage extends AppCompatActivity {

    private FirebaseFirestore db;
    private RecyclerView mFirestoreList;
    private FirestoreRecyclerAdapter adapter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_homepage);


        db = FirebaseFirestore.getInstance();
        mFirestoreList = findViewById(R.id.firestore_list);
        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        // Query

        Query query = db.collection("avatar").whereEqualTo("currentHardwareId", androidId);
        Log.d("csdf", "test");

        // RecyclerOptions

        FirestoreRecyclerOptions<Avatar> options = new FirestoreRecyclerOptions.Builder<Avatar>()
                .setQuery(query, Avatar.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Avatar, AvatarViewHolder>(options) {


            @NonNull
            @Override
            public AvatarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_avatar, parent, false);
                return new AvatarViewHolder(view);
            }


            @Override
            protected void onBindViewHolder(@NonNull AvatarViewHolder holder, int position, @NonNull Avatar model) {

                    holder.avatar_name.setText("Romain");
                    holder.avatar_distance.setText("Distance parcourue : " + "1 km");
                    holder.avatar_temps.setText("Durée : " + "26 minutes");
                    holder.avatarImage.setImageResource(R.mipmap.avatar);

            }
        };

        mFirestoreList.setHasFixedSize(true);
        mFirestoreList.setLayoutManager(new LinearLayoutManager(this));
        mFirestoreList.setAdapter(adapter);
        adapter.startListening();




    }

    private class AvatarViewHolder extends RecyclerView.ViewHolder{

        private TextView avatar_name;
        private TextView avatar_position;
        private TextView avatar_distance;
        private TextView avatar_temps;
        private ImageView avatarImage;

        public AvatarViewHolder(@NonNull View itemView) {
            super(itemView);

            avatar_name = itemView.findViewById(R.id.avatar_name);
            avatar_position = itemView.findViewById(R.id.avatar_name);
            avatar_distance = itemView.findViewById(R.id.avatar_distance);
            avatar_temps = itemView.findViewById(R.id.avatar_temps);
            avatarImage = itemView.findViewById(R.id.avatarImage);

        }
    }

    public void onClickButtonInfos(View view){
        Intent k = new Intent(Homepage.this, InfoAvatar.class);
        startActivity(k);
    }

}
