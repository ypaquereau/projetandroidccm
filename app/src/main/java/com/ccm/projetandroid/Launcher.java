package com.ccm.projetandroid;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

public class Launcher extends AppCompatActivity {

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        db = FirebaseFirestore.getInstance();

        CollectionReference collectionReference = db.collection("avatar");
        collectionReference.whereEqualTo("ownerHardwareId", androidId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        Map<Object, String> map = new HashMap<>();
                        map.put("isActive", "Active");
                        collectionReference.document(document.getId()).set(map, SetOptions.merge());
                    }
                }
            }
        });

        db.collection("avatar")
                .whereEqualTo("ownerHardwareId", androidId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.getResult().size() >= 1){

                            Intent k = new Intent(Launcher.this, Homepage.class);
                            startActivity(k);
                            finish();
                        } else {
                            Intent k = new Intent(Launcher.this, MainActivity.class);
                            startActivity(k);
                            finish();
                        }
                    }
                });
    }
}
