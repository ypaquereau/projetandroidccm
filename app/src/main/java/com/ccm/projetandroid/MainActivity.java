package com.ccm.projetandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity {

    private EditText editName;
    private EditText editLimitTime;
    private EditText editLimitDistance;
    private EditText editRefreshLocationFrequency;
    private FirebaseFirestore db;
    private LocationHelper locationHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        locationHelper = new LocationHelper(this);

        db = FirebaseFirestore.getInstance();

        db.collection("avatar")
                .whereEqualTo("ownerHardwareId", androidId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.getResult().size() >= 1){

                            Intent k = new Intent(MainActivity.this, Homepage.class);
                            startActivity(k);
                        }
                    }
                });

        editName = findViewById(R.id.form_avatar_name);
        editLimitTime = findViewById(R.id.form_avatar_limit_time);
        editLimitDistance = findViewById(R.id.form_avatar_limit_distance);
        editRefreshLocationFrequency = findViewById(R.id.form_avatar_refresh_location_frequency);
    }

    public void onClickSubmitForm(View v) {
        String name = this.editName.getText().toString();
        String limitTime = this.editLimitTime.getText().toString();
        String limitDistance = this.editLimitDistance.getText().toString();
        String refreshLocationFrequency = this.editRefreshLocationFrequency.getText().toString();
        String isActive = "Active";

        if (name.matches("") || limitTime.matches("") || limitDistance.matches("") || refreshLocationFrequency.matches("")) {
            Toast.makeText(this, R.string.all_fields_filled, Toast.LENGTH_SHORT).show();
        }
        else {
            String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

            if (locationHelper.canGetLocation()) {
                locationHelper.refreshLocation();
                double latitude = locationHelper.getLatitude();
                double longitude = locationHelper.getLongitude();

                this.insertAvatar(new Avatar(name, androidId, Integer.parseInt(limitTime), Integer.parseInt(limitDistance), Integer.parseInt(refreshLocationFrequency), latitude, longitude, isActive));
            } else{
                locationHelper.showSettingsAlert();
            }
        }
    }

    private void insertAvatar(Avatar avatar) {
        CollectionReference dbAvatar = db.collection("avatar");

        dbAvatar.add(avatar).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(MainActivity.this, R.string.success_register_avatar, Toast.LENGTH_SHORT).show();
                Intent k = new Intent(MainActivity.this, Homepage.class);
                startActivity(k);
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, R.string.error_register_avatar, Toast.LENGTH_SHORT).show();
            }
        });
    }
}