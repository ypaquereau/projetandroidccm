package com.ccm.projetandroid;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import android.location.Location;

public class LocationHelper {
    private GpsTracker gpsTracker;
    private Context mContext;
    private double latitude;
    private double longitude;

    public LocationHelper(Context context) {
        mContext = context;
        gpsTracker = new GpsTracker(mContext);
    }


    public void refreshLocation() {
        if (gpsTracker.canGetLocation()) {
            gpsTracker.getLocation();
            this.latitude = gpsTracker.getLatitude();
            this.longitude = gpsTracker.getLongitude();
            this.gpsTracker.stopUsingGPS();
        } else {
            this.gpsTracker.showSettingsAlert();
        }
    }

    /**
     * Return distance between two locations in meters
     * @param lat
     * @param lon
     * @return float
     */
    public float getDistance(double lat, double lon) {
        Location currentLocation = new Location("new");
        currentLocation.setLatitude(this.latitude);
        currentLocation.setLongitude(this.longitude);

        Location oldLocation = new Location("old");
        oldLocation.setLatitude(lat);
        oldLocation.setLongitude(lon);

        return currentLocation.distanceTo(oldLocation);
    }

    public boolean canGetLocation() {
        return this.gpsTracker.canGetLocation();
    }

    public void showSettingsAlert() {
        this.showSettingsAlert();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCompleteAddressString(double lat, double lon) throws IOException {
        Geocoder geocoder = new Geocoder(this.mContext, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
        return addresses.get(0).getLocality();
    }
}
