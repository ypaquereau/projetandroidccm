package com.ccm.projetandroid;

public class Trip {
    private String recipient;
    private float distance;
    private int time;
    private double latitude;
    private double longitude;

    private Trip() {}

    public Trip(String recipient, float distance, int time, double latitude, double longitude) {
        this.recipient = recipient;
        this.distance = distance;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
