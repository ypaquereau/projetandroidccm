package com.ccm.projetandroid;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;


public class InfoAvatar extends AppCompatActivity {

    private FirebaseFirestore db;
    private RecyclerView mFirestorelist;
    private FirestoreRecyclerAdapter adapter;
    TextView nomAvatar, distanceTotale, tempsTotal, position_actuelle;
    ImageView imageView;
    private LocationHelper locationHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_avatar);
        locationHelper = new LocationHelper(this);
        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        db = FirebaseFirestore.getInstance();

        nomAvatar = findViewById(R.id.nomAvatar);
        distanceTotale = findViewById(R.id.distanceTotale);
        tempsTotal = findViewById(R.id.tempsTotal);
        imageView = findViewById(R.id.imageView);
        position_actuelle = findViewById(R.id.position_actuelle);

        Query query = db.collection("avatar").whereEqualTo("ownerHardwareId", androidId);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException error) {
               if(error != null){
                   Log.w("erreurInfoAvatar", "error : ", error);
               }

               for (DocumentChange dc : snapshots.getDocumentChanges()){

                   try {
                       String ville = locationHelper.getCompleteAddressString(dc.getDocument().getDouble("latitude"), dc.getDocument().getDouble("longitude"));
                   nomAvatar.setText(dc.getDocument().getData().get("name").toString());
                   position_actuelle.setText("Ville actuelle : " + ville);
                   distanceTotale.setText("Distance totale parcourue : " + "");
                   tempsTotal.setText("Temps total de voyage : " + "");
                   imageView.setImageResource(R.mipmap.avatar);
                   } catch (IOException e) {
                       e.printStackTrace();
                   }

               }
            }
        });
    }
}
